from pybuilder.core import use_plugin
from pybuilder.core import init
from pybuilder.core import Author
from pybuilder.core import task
from pybuilder.core import use_bldsup
import os
use_bldsup(build_support_dir=os.path.join('src', 'main', 'python'))
import ess2bmp

use_plugin("python.core")

default_task = "publish"


@init
def initialize(project):
    project.name = 'ESS to BMP'
    project.version = '1.0'
    project.summary = 'Project for CS3280'
    project.description = '''Given a Skyrim save file or a directory of skyrim save files, generate thumbnail(s)'''
    project.authors = [Author('John Chittam', 'jchitta2@my.westga.edu')]
    project.license = "not for redistribution"
    project.url = 'https://cs.westga.edu'


@task
def convertAllIn(project):
    directory = project.get_property('directory')
    ess2bmp.generateBmpsFromDirectory(directory)
