import sys
from pathlib import Path


def main(saveFilePath, bmpPath):
    generateBmp(saveFilePath, bmpPath)


def generateBmp(saveFilePath, bmpPath):
    if getFilePath(saveFilePath).suffix != '.ess':
        raise ValueError('file must be a .ess file')

    saveFile = open(getFilePath(saveFilePath), 'rb')
    offsetToShotWidth = getOffsetToShotWidth(saveFile)
    shotHeight = getShotHeight(saveFile, offsetToShotWidth)
    shotWidth = getShotWidth(saveFile, offsetToShotWidth)
    print(f"Width = {shotWidth}, Height = {shotHeight}")
    bmpOutFile = open(getFilePath(bmpPath), 'wb+')

    for data in getBmpHeaderData(shotHeight, shotWidth):
        bmpOutFile.write(data)

    for row in getImageData(saveFile, shotHeight, shotWidth, offsetToShotWidth + 8):
        for byte in row:
            bmpOutFile.write(byte)

    bmpOutFile.close


def getBmpHeaderData(shotHeight, shotWidth):
    imageSizeInBytes = shotWidth * shotHeight * 3
    headerData = [
        b"BM",                                                    # signature
        (imageSizeInBytes + 54).to_bytes(4, byteorder='little'),  # file size
        (0).to_bytes(2, byteorder='little'),                      # reserved
        (0).to_bytes(2, byteorder='little'),                      # reserved
        (54).to_bytes(4, byteorder='little'),                     # data offset
        (40).to_bytes(4, byteorder='little'),                     # file header size
        shotWidth.to_bytes(4, byteorder='little'),                # width in px
        shotHeight.to_bytes(4, byteorder='little'),               # height in px
        (2).to_bytes(2, byteorder='little'),                      # number of planes
        (24).to_bytes(2, byteorder='little'),                     # bits per px
        (0).to_bytes(4, byteorder='little'),                      # compression (0 == false)
        imageSizeInBytes.to_bytes(4, byteorder='little'),         # image size
        (0).to_bytes(4, byteorder='little'),                      # XpixelsPerM (0 == no preference)
        (0).to_bytes(4, byteorder='little'),                      # YpixelsPerM (0 == no preference)
        (3).to_bytes(4, byteorder='little'),                      # colors used
        (3).to_bytes(4, byteorder='little')                       # important colors
    ]

    return headerData


def getImageData(saveFile, shotHeight, shotWidth, offsetToImageBytes):
    imageBytes = []
    saveFile.seek(offsetToImageBytes)

    for row in range(0, shotHeight):
        rowBytes = []
        for byte in range(0, shotWidth):
            blue = saveFile.read(1)
            green = saveFile.read(1)
            red = saveFile.read(1)
            rowBytes.append(red)
            rowBytes.append(green)
            rowBytes.append(blue)
        imageBytes.insert(0, rowBytes)

    return imageBytes


def getShotHeight(saveFile, offsetToShotWidth):
    saveFile.seek(offsetToShotWidth + 4)
    return int.from_bytes(saveFile.read(4), byteorder='little')


def getShotWidth(saveFile, offsetToShotWidth):
    saveFile.seek(offsetToShotWidth)
    return int.from_bytes(saveFile.read(4), byteorder='little')


def getOffsetToShotWidth(saveFile):
    saveFile.seek(25)
    playerNameLen = int.from_bytes(saveFile.read(2), byteorder='little')
    saveFile.seek(playerNameLen + 4, 1)
    locationLen = int.from_bytes(saveFile.read(2), byteorder='little')
    saveFile.seek(locationLen, 1)
    gameDateLen = int.from_bytes(saveFile.read(2), byteorder='little')
    saveFile.seek(gameDateLen, 1)
    playerRaceEditorIdLen = int.from_bytes(saveFile.read(2), byteorder='little')
    return saveFile.tell() + playerRaceEditorIdLen + 18


def generateBmpsFromDirectory(directory):
    path = Path(directory)
    files = list(path.rglob('*.ess'))

    if len(files) == 0:
        raise ValueError('no .ess files found in directory')
    for file in files:
        outPath = f"{path}/{file.stem}.bmp"
        generateBmp(str(file), outPath)


def getFilePath(path):
    path = path.replace('\\', '/')
    return Path(path)


if __name__ == '__main__':
    saveFilePath = sys.argv[1]
    bmpPath = sys.argv[2]
    main(saveFilePath, bmpPath)
